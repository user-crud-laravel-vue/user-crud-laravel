<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserStore;
use App\Http\Requests\UserUpdate;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use App\Repositories\User\Contracts\UserRepositoryInterface;

class UsersController extends Controller
{
    /**
     * @var UserRepositoryInterface
     */
    private $repository;

    /**
     * UsersController constructor.
     * @param UserRepositoryInterface $repository
     */
    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $users = $this->repository->list();

        return response()->json($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserStore $request
     * @return JsonResponse
     */
    public function store(UserStore $request)
    {
        $user = $this->repository->create($request->validated());

        return response()->json($user, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $user
     * @return JsonResponse
     */
    public function show($user)
    {
        $userFromDb = $this->repository->findById($user);

        return response()->json($userFromDb);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdate $request
     * @param int $user
     * @return JsonResponse
     */
    public function update(UserUpdate $request, $user)
    {
        $updatedUser = $this->repository->update($user, $request->validated());

        return response()->json($updatedUser);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $user
     * @return JsonResponse
     */
    public function destroy($user)
    {
        $this->repository->delete($user);

        return response()->json([
            'message' => 'User Deleted',
        ]);
    }
}
