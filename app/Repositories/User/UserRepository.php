<?php

namespace App\Repositories\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Hash;
use App\Repositories\User\Contracts\UserRepositoryInterface;
use phpDocumentor\Reflection\Types\Boolean;

class UserRepository implements UserRepositoryInterface
{

    /**
     * List all users
     *
     * @return Collection
     */
    public function list()
    {
        $perPage = request()->has('perPage') ? request('perPage') : 10;

        $users = User::query();

        if (request()->has('filter')) {
            $filter = '%' . request('filter') . '%';

            $users->where('name', 'LIKE', $filter)
                ->orWhere('email', 'LIKE', $filter)
                ->orWhere('address', 'LIKE', $filter)
                ->orWhere('username', 'LIKE', $filter);
        }

        return $users->orderByDesc('id')->paginate($perPage);
    }

    /**
     * Find a user by id
     * @param $id
     * @return Collection
     */
    public function findById($id)
    {
        return User::findOrFail($id);
    }

    /**
     * Update a given user
     *
     * @param $id
     * @param $dataToUpdate
     * @return Collection
     */
    public function update($id, $dataToUpdate)
    {
        $user = $this->findById($id);

        if (isset($dataToUpdate['password'])) {
            $dataToUpdate['password'] = Hash::make($dataToUpdate['password']);
        }

        $user->update($dataToUpdate);

        return $user;
    }

    /**
     * Create new user
     *
     * @param $data
     * @return Collection
     */
    public function create($data)
    {
        $data['password'] = Hash::make($data['password']);

        return User::create($data);
    }

    /**
     * Delete a given user
     * @param $id
     * @return Boolean
     */
    public function delete($id)
    {
        $user = $this->findById($id);

        return $user->delete();
    }
}
