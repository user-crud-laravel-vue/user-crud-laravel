<?php

namespace App\Repositories\User\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface UserRepositoryInterface
{
    /**
     * List all users
     *
     * @return Collection
     */
    public function list();

    /**
     * Find a user by id
     *
     * @param $id
     * @return Collection
     */
    public function findById($id);

    /**
     * Update a given user
     *
     * @param $id
     * @param $dataToUpdate
     * @return Collection
     */
    public function update($id, $dataToUpdate);

    /**
     * Create new user
     *
     * @param $data
     * @return Collection
     */
    public function create($data);

    /**
     * Delete a given user
     *
     * @param $id
     * @return Boolean
     */
    public function delete($id);
}
